### Install package by executing:
```
npm install temp-email --save
```

### Declare package in sources
```
var TempEmail = require('temp-email');
```

### To generate new email address:
Example:
```
TempEmail.generate().then(newEmail=>{
  console.log(newEmail); //caae4bd9-38e0-9ac3-cbb2-8736cf06eda0@zainmax.net
});
```

### To check email inbox:
Example:
```
TempEmail.getLastLetterText('891a7078-cdcd-5a70-b407-faff584d30c3@9me.site', { timeout: 10000 }).then(resp => {
    console.log(`Success: ${resp}`);
}).catch(err => {
    console.error(err);
});
```

### If you are interested only in part of email then you may use css selector to filter response:
Example:
```
TempEmail.getLastLetterText('891a7078-cdcd-5a70-b407-faff584d30c3@9me.site', { timeout: 10000, cssSelector:'.verification-code'}).then(resp => {
    console.log(`Success: ${resp}`);
}).catch(err => {
    console.error(err);
});
```