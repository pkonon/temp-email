const mail = require('temp-mail');
const jsdom = require('jsdom');

module.exports = class TempEmail {
    static generate() {
        return mail.generateEmail();
    }

    static getLastLetterText(email, { timeout = 0, cssSelector } = {}) {
        return TempEmail._getEmailText(email, timeout).then(text => {
            if (!cssSelector) {
                return text;
            }
            return TempEmail._selectText(cssSelector, text);
        });
    }

    static _selectText(cssSelector, text) {
        return new Promise((resolve, reject) => {
            jsdom.env(
                text, [],
                function(errors, window) {
                    try {
                        var code = window.document.querySelector(cssSelector).innerHTML;
                        resolve(code);
                    } catch (e) {
                        reject(e);
                    }
                    if (window) {
                        window.close();
                    }
                }
            );
        });
    }

    static _getEmailText(emailAddress, timeout) {
        var endDate = Date.now() + timeout;
        return mail.getInbox(emailAddress).then(inbox => {
            return inbox.reduce((memo, item) => {
                if (memo.mail_timestamp < item.mail_timestamp) {
                    return item
                }
                return memo;
            }, { mail_timestamp: 0 }).mail_text_only;
        }).catch(err => {
            if (endDate <= Date.now()) {
                throw err;
            }
            return new Promise(resolve => {
                setTimeout(() => {
                    resolve(TempEmail._getEmailText(emailAddress, endDate - Date.now()));
                }, 1000);
            });
        });
    }
}
